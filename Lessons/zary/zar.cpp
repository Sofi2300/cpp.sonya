// -=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Законы формальной логики и Де Моргана в примерах
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include<iostream>
using namespace std;

int main(){
	cout << boolalpha;
	/*
	bool var = false;
	var = !var;
	bool a = true, b = false;
	cout << var << "\n";
	cout << (a == b) << "\n";
	cout << (a && b) << "\n";
	cout << (a || b) << "\n";
	cout << (a != b) << "\n";
	*/
	bool a, b, result;
	cout << "Введите значение первой переменной: ";
	cin >> a;
	cout << "Выбрать логическую операцию (1 - &&, 2 - ||, 3 - ^, 4 - ==, 5 - !, 6 - ): ";
	int choose = 0;
	cin >> choose;
	cout << "Введите значение второй переменной: ";
	cin >> b;
	switch (choose){
	case 1:
         result = (a && b); // И	
		break;
	case 2:
		result = (a || b); // ИЛИ
		break;
	case 3:
	    result = (a ^ b); // ИСКЛЮЧАЮЩАЯ ИЛИ
		break;
	case 4:
		result = (a == b); // РАВНО
 		break;
	case 5:s
		result = (a != b); // НЕ
		break;    		
	default:
		cout << "Try again!";
		return main();
	}
	cout << result << "\n";
	return 0;
	
	
}
// Output:
/*
true
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
