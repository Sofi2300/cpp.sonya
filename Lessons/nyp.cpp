// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// простой пример цикла завершения или продолжения выполнения программы
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-==-=-=-=-=-=-=-=//
#include<iostream>
using namespace std;

int main(){
	char ch = 'a';
	int count = 1; // Нумерация строк
	while (ch <= 'z') {
		cout << "#" << count << ": " << ch <<  "-" << static_cast<int>(ch) << " ";
		++ch;
		if (count % 3 == 0) {// Для перехода на новую строку
			cout << "\n" << endl;
		}
		++count;
	}
	cout << "\n";
	return 0;
}
// Output:
/*
*/
