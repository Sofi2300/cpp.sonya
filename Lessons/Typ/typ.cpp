// -=-=-=-=-=-=--=-=-=-=-=-=---=-=--=--=-=-=-=//
// булевый тип данных. простые примеры. 
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include<iostream>
using namespace std;

int main(){
	cout << boolalpha;
	bool a = false; // 0 - false, 1 - true
	bool b = true;
    cout << a << "\t" << b << "\n";
	int aa = 5, bb = 12;
	bool result = (aa == bb) ? true : false; // == -Оператор сравнения
	cout << "аа равно bb?" << result << "\n";
	result = (aa < bb) ? true : false; // оператор сравнения "меньше"
	cout << "aa меньше bb? " << result << "\n";
	result = (aa > bb) ? true : false; // оператор сравнения "больше"
	cout << "aa ,больше bb? " << result << "\n";
	result = (aa <= bb) ? true : false; // < - оператор сравнения "меньше и равно"
	cout << "aa ,меньше и равно bb? " << result << "\n";
	result = (aa >= bb) ? true : false; // > - оператор сравнения "больше и равно"
	cout << "aa ,больше и равно bb? " << result << "\n";
	result = (aa != bb) ? true : false;
	cout << "aa не равно bb? " << result << "\7" << "\n";
	result = (aa < bb) || (aa > bb); // аа меньше bb ИЛИ аа больше bb. 1 ИЛИ 1 = true, 0 ИЛИ 0 = false, 1 ИЛИ 0 = true;
	cout << "Сравнения ИЛИ" << result << "\n";
	result = (aa < bb) && (aa > bb); // 1 И 1 = true, 0 И 0 = false, 1 И 0 = false;
	cout << "Сравнение логической И" << result << "\n";
	result = (aa != bb) xor (aa < bb);
	cout << "сраснение Логическое исключающее ИЛИ XOR" << result << "\n";s

	return 0;
}
// Output
/*
false	true
аа равно bb?false
aa меньше bb? true
aa ,больше bb? false
aa ,меньше и равно bb? true
aa ,больше и равно bb? false
aa не равно bb? true
Сравнения ИЛИtrue
Сравнение логической Иfalse
сраснение Логическое исключающее ИЛИ XORtrue
*/
// -=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
