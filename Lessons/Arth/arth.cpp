// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Примеры решения математических задач на С++
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include<iostream>
using namespace std;

int main(){
   cout << "Найти среднее арифметическое";
   int result = 0, firstNum = 0, secondNum = 0, thirdNum = 0;
   cout << "Введите три гатуральных числа: ";
   cin >> firstNum >> secondNum >> thirdNum;
   result = (firstNum + secondNum + thirdNum) / 3;
   cout << "Среднее арифметическое: " << result << "\n";

   cout << "Найти общее сопротивление последовательных резисторов\n";
   cout << "Введите значения двух резисторов: ";
   float totalResist = 0, firstResist = 0, secondResist = 0;
   cin >> firstResist >> secondResist;
   totalResist = firstResist + secondResist;
   cout << "Общие сопротивление резисторов: " << totalResist << " Ом " << endl;

   

   
   return 0;	
}
// Output:
/*

*/
// -==-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=// 
