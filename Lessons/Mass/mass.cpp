// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// 2-x merniy massiv
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
#include<iostream>
using namespace std;

int main(){
	//char ch[] = {'a', 'b', 'c'};
	const int ARR_COL_SIZE = 3, ARR_ROW_SIZE = 3;
	int multiArr[ARR_ROW_SIZE][ARR_COL_SIZE] = {{12, 30, 45,},{78, 96, 101}};
	cout <<  "Dostup k elementam 2-x mernogo massiva obuchnum sposobom: ";
	cout << multiArr[0][0] << ", "; // 1-y ryada 1-y kolonki
	cout << multiArr[0][1] << ", "; // 1-y ryada 2-y kolonki
	cout << multiArr[0][2] << ", "; // 1-y ryada 3-y kolonki
	cout << multiArr[1][0] << ", "; // 2-y ryada 1-y kolonki
	cout << multiArr[1][1] << ", "; // 2-y ryada 2-y kolonki
	cout << multiArr[1][2] << ", "; // 2-y ryada 3-y kolonki
	cout << endl;
	cout << "Dostup k elementam 2-x mernogo massiva pereborom (for: )";
	for (int row = 0; row < 2; ++row) {
		for (int col = 0; col < 3; ++col){
			cout << multiArr[row][col] << " ";
		}
		cout << "\t";
	} 
	cout << "\nPoluchenie x/y koordinat\n";
	cout << "X: ";
	for (int x = 0; x < 3 ; ++x) {
		cout << multiArr[0][x] << " ";
	}
	cout << "\nY: ";
	for (int y = 0; y < 3; ++y) {
		cout << multiArr[1][y] << " ";
	}
	return 0;
}
// Output
/*
Dostup k elementam 2-x mernogo massiva obuchnum sposobom: 12, 30, 45, 78, 96, 101, 
Dostup k elementam 2-x mernogo massiva pereborom (for: )12 30 45 	78 96 101 	
Poluchenie x/y koordinat
X: 12 30 45 
Y: 78 96 101 
*/
