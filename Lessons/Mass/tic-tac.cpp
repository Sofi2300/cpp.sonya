// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// 2-x merniy massiv
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
#include<iostream>
using namespace std;

int main(){
	const int ROWS = 3, COLUMNS = 3;
    char board[ROWS][COLUMNS] = {{'O', 'X', 'O'},
                                 {' ', 'X', 'X'},
                                 {'X', 'O', 'O'}};
    cout << "Doska dlya igru v krestiki-noliki:\n";
    for (int i = 0; i < ROWS; ++i){
    	for (int j = 0; j < COLUMNS; ++j){
 	    cout << board[i][j];
    	}
    	cout << endl;
    }
    cout << "\nPomeshaem 'X' v svobodnuyu kletku\n\n";
    board[1][0] = 'X';
    cout << "Igrovaya doska teper vuglyadit tak\n";  
    for (int i = 0; i < ROWS; ++i){
          	for (int j = 0; j < COLUMNS; ++j){
       	    cout << board[i][j];
          	}
          	cout << endl;  
    }
    cout << "\nPobedill igrok X!\n";
    return 0;
 }
// Output
/*

*/
