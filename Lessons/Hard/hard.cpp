#include<iostream>
#include<limits.h>
using namespace std;

int main() {
	char chMin = SCHAR_MIN;
	char chMax = SCHAR_MAX;
	unsigned char uChMax = UCHAR_MAX;
	short shortMin = SHRT_MIN;
	short shortMax = SHRT_MAX;
	unsigned short uShortMax = USHRT_MAX;
	int intVarMin = INT_MIN;
	int intVarMax = INT_MAX;
	unsigned int uIntVarMax = UINT_MAX;
	long int longVarMin = LONG_MIN;
	long int longVarMax = LONG_MAX;
	unsigned long uLongVarMax = ULONG_MAX;
	long long longLongVarMin = LLONG_MIN;
	long long longLongVarMax = LLONG_MAX;
	unsigned long long uLongLongVarMax =ULLONG_MAX;
	cout << "Char: " << static_cast<int>(chMin) <<" : " << static_cast<int> (chMax) << endl;
	cout << "Unsigned Char maximum: " << static_cast<int>(uChMax) << endl;
	cout << "Short: " << shortMin << " : " << shortMax << endl;
	cout << "Unsigned Short maximum: " << static_cast<int>(uShortMax) << endl;
	cout << "Int: " << intVarMin << " : " << intVarMax << endl;
	cout << "Unsigned Int maximum:" << uIntVarMax << endl;
	cout << "Long: " << longVarMin << " : " << longVarMax << endl;
	cout << "Unsigned long maximum: " << uLongVarMax << endl;
	cout << "Long Long: " << longLongVarMin << " : " << longLongVarMax  << endl;
	cout << "Unsigned Long Long maximum:" << uLongLongVarMax << endl;

	return 0;
}