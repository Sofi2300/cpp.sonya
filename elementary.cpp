// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// Примеры строк, работы с ними, а так же элементарная арифметика
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include<iostream>
#include<unistd.h>

using namespace std;

int main(){
    cout << boolalpha;
    char strSonya[] = {"Sonya"};
    char strProg[] = {"Programmer"};
    cout << strSonya << "\t" << strProg << endl;
    cout << (sizeof(strSonya) / sizeof(strSonya[0])) << "\n";
    cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
    string strSonyaString = "Sonya";
    string strSonyaProgramString = " пианист";
    cout << strSonyaString + strSonyaProgramString << "\n";
     cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
     int a = 5, b = 6;
     int result = a + (b - 3);
     cout << result << "\n";
     cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
     int aa = 5, bb = 6;
     int res = aa * bb;
     cout << "Результат вычисления aa * bb = " << res << "\n";
     int aaa = 12, bbb = 2;
     int devision = aaa / bbb;
     cout << "Результат деления aaa / bbb = " << devision << "\n";
     int val = 6 + 5 - 1 * 5 / 2;
     cout << val << "\n";
     cout <<"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
     int a2 = 5, b2 = 6;
     cout << (a2 += b2) << endl;
     cout << (a2 -= b2) << "\n";
     cout << (a2 *= b2) << "\n";
     cout << (a2 /= b2) << "\n";
     cout << ((20 % 7) ? false : true)<< "\n";
     sleep(1);
    cout << "Сигнал с задержкой" << "\a" << endl;

	return 0;
}
// Output:
/*
Sonya	Programmer
6
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Sonya пианист
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
8
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Результат вычисления aa * bb = 30
Результат деления aaa / bbb = 6
9
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
11
5
30
5
false
Сигнал с задержкой

*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
