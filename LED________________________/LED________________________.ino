void setup() {
  Serial.begin(9600);
  pinMode (10, OUTPUT);
  pinMode (9, OUTPUT);
  pinMode (11, OUTPUT);
}

void loop() {
  analogWrite (9, 0); // R
  analogWrite (10, 0); // G
  analogWrite (11, 0); // B
  for (int i = 0; i <= 255; i += 5) {
    analogWrite (10, i);
    delay(50);
    Serial.println(i);
  }
  for (int i = 255; i >= 0; i -= 5) {
    analogWrite (10, i);
    delay(50);
    Serial.println(i);
  }
}
